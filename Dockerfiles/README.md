## How to install Docker
Docker Engine is available on a variety of [Linux platforms](https://docs.docker.com/engine/install/#server), [MacOS](https://docs.docker.com/docker-for-mac/install/) and [Windows 10](https://docs.docker.com/docker-for-windows/install/) 

This documentation contains information on installing Docker Desktop on **ubuntu**, If you are looking for information about installing Docker in another operating system, see https://docs.docker.com/engine/install/

* Before you install Docker for the first time on a new host machine, you need to set up the Docker repository.  
```shell
$ sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
```
* Update the apt package, and install the latest version of Docker.

```shell
 $ sudo apt-get update
 $ sudo apt-get install docker-ce docker-ce-cli containerd.io
```
* Test the installation.
```shell 
$ docker --version
```

## Container shell access
```bash
docker run --rm -v "$(pwd)":/home/build/openwrt -it librerouter/librerouteros-builder:latest bash
```


